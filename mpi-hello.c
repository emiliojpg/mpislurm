#include <stdio.h>
#include <mpi.h>

const char *mthr_support[] = {"MPI_THREAD_SINGLE", "MPI_THREAD_FUNNELED", "MPI_THREAD_SERIALIZED", "MPI_THREAD_MULTIPLE"};

int main(int argc, char *argv[])
{
  char name[BUFSIZ];
  int length, myid, nprocs;
  int threadlevel = 0;
	
  //MPI_Init(&argc, &argv);
  MPI_Init_thread (&argc, &argv, MPI_THREAD_MULTIPLE, &threadlevel);
  MPI_Get_processor_name(name, &length);
  MPI_Comm_rank (MPI_COMM_WORLD, &myid);
  MPI_Comm_size (MPI_COMM_WORLD, &nprocs);

  if (myid < 0) {
    MPI_Abort (MPI_COMM_WORLD, 1);
    return -1;
  }

  if (myid == 0) {
    printf("Number or processes (MPI Ranks): %d\n", nprocs);
    printf("Threading level supported: %s\n", mthr_support[threadlevel]);
  }

  printf("%s: Hello world from P%d\n", name, myid); fflush (stdout);
  MPI_Finalize();
	
  return 0;
}
