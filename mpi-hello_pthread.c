#include <stdio.h>
#include <mpi.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/syscall.h>

const char *mthr_support[] = {"MPI_THREAD_SINGLE", "MPI_THREAD_FUNNELED", "MPI_THREAD_SERIALIZED", "MPI_THREAD_MULTIPLE"};

struct thr_param {
  int mpi_rank;
  char *node_name;
};

void *auxiliary_func( void *param )
{
  struct thr_param *myid = (struct thr_param *) param;
  pid_t thr_id = syscall(__NR_gettid);

  printf("%s: Hello world from P%d-thr#%d\n", myid->node_name, myid->mpi_rank, thr_id); fflush (stdout);

  return 0;
}

int main(int argc, char *argv[])
{
  char name[BUFSIZ];
  int length, myid, nprocs;
  int nthreads = 1, threadlevel = 0;
  pthread_t tid;

  if (argc > 1)
    nthreads = atoi(argv[1]);

  //MPI_Init(&argc, &argv);
  MPI_Init_thread (&argc, &argv, MPI_THREAD_MULTIPLE, &threadlevel);
  MPI_Get_processor_name(name, &length);
  MPI_Comm_rank (MPI_COMM_WORLD, &myid);
  MPI_Comm_size (MPI_COMM_WORLD, &nprocs);

  if (myid < 0) {
    MPI_Abort (MPI_COMM_WORLD, 1);
    return -1;
  }

  if (myid == 0) {
    printf("Number or processes (MPI Ranks): %d\n", nprocs);
    printf("Threading level supported: %s\n", mthr_support[threadlevel]);
  }

  struct thr_param param = {myid, name};

  for (int i = 1; i < nthreads; i++) {
    if (pthread_create(&tid, NULL, auxiliary_func, (void *)&param) != 0) {
      printf ("Problem spawning new thread!\n"); fflush (stdout);
      return -1;
    }
  }

  pid_t thr_id = syscall(__NR_gettid);
  printf("%s: Hello world from P%d-thr#%d (main thread)\n", name, myid, thr_id); fflush (stdout);

  if (nthreads > 1)
    pthread_join(tid, 0);

  MPI_Finalize();
	
  return 0;
}
