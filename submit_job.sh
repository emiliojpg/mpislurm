#!/bin/bash
#SBATCH -t 00:20:00 # execution time hh:mm:ss *OB*
#SBATCH --mem 32768
#SBATCH -n 4 #total tasks (MPI processes, i.e. MPI Ranks)
#SBATCH --ntasks-per-node 1 # max ntasks per node
#SBATCH -c 16 #cores/task (shared-mem threads/process)
##SBATCH -N 8 #nodes (can be obtained from the two previous)
##SBATCH --ntasks-per-core ntasks # max ntasks per core
##SBATCH --ntasks-per-socket ntasks # max ntasks per socket
##SBATCH -p cola-corta

echo This is task $SLURM_JOB_ID
echo executed on `date`
echo in `pwd` at `hostname`

echo
echo Nodos de ejecución: ${SLURM_JOB_NUM_NODES} \(${SLURM_JOB_NODELIST}\)
echo Número de procesos \(MPI Ranks\): ${SLURM_NTASKS}
echo "  -> tareas/nodo: ${SLURM_TASKS_PER_NODE}"
echo Hilos por proceso: ${SLURM_CPUS_PER_TASK}
echo Cola: ${SLURM_JOB_PARTITION}

BINS="mpi-hello mpi-hello_pthread"

# Load a most recent GCC compiler than default
module load gnu8

# Load openmpi module
#module load openmpi3/3.1.0
module load openmpi3/3.1.4
#module load openmpi4/4.0.5

# Check GCC version
GCC_ver=`mpicc --version | head -n 1`
echo -e "\n* Compiling with GCC: ${GCC_ver}"

# Check MPI version
MPI_ver=`mpirun --version | head -n 1`
echo -e "\n* MPI version: ${MPI_ver}"

# Check MPI multi-threading support
MPI_mthr=`ompi_info | grep -i thread`
echo -e "  -> ${MPI_mthr#${MPI_mthr%%[![:space:]]*}}"

# Check available threads in node
echo -e "\n* Threads in node: $(nproc)\n"

rm -f ${BINS} *.o *~
make ${BINS} CC=mpicc CFLAGS="-O2 -Wall -Wextra"

# single-thread bin
exe=mpi-hello
for np in $(seq ${SLURM_NTASKS})
do
    echo -e "\n* TEST " ${exe} " - " ${np}
    echo -e "mpirun -np ${np} ./${exe}"
    mpirun -np ${np} ./${exe}
done

# multi-thread bin
THR="1 2 4 8 16"

exe=mpi-hello_pthread
for np in $(seq ${SLURM_NTASKS})
do
    for thr in $THR
    do
	echo -e "\n* TEST " ${exe} " - " ${np} " - " ${thr}
	echo -e "mpirun -np ${np} ./${exe} ${thr}"
	mpirun -np ${np} ./${exe} ${thr}
    done
done
